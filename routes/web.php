<?php

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('detalle/{product}', 'HomeController@detail')->name('detail');
Route::get('categoria/{category}', 'HomeController@category')->name('category');



Route::resource('categories', 'Admin\CategoryController');