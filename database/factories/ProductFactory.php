<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    return [
        'name' => $name = $faker->sentence(3),
        'slug' => str_slug($name),
        'description' => $faker->sentence(20),
        'price' => $faker->randomFloat(2,100,500),
        'image' => 'default.jpg',
        'category_id' => \App\Category::all()->random()->id
    ];
});
