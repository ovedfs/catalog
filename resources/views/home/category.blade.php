@extends('layouts.app')

@section('content')
<div class="container">
	<h2>Categoría: {{ $category->name }} <span class="badge badge-success">{{ $category->products->count() }} productos</span></h2>
	<div class="card-columns">
		@forelse($category->products as $product)
			@include('home.card')
		@empty
			<p>No hay productos registrados...</p>
		@endforelse
	</div>
</div>
@endsection
