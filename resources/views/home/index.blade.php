@extends('layouts.app')

@section('content')
<div class="container">
	<div class="card-columns">
		@forelse($products as $product)
			@include('home.card')
		@empty
			<p>No hay productos registrados...</p>
		@endforelse
	</div><hr>
	<div class="text-center">
		{{ $products->links() }}
	</div>
</div>
@endsection
