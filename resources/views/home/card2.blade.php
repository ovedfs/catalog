<div class="card bg-light">
	<div class="card-header">
		<h3>{{ $product->name }}</h3>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-md-6">
				<img class="img-fluid img-thumbnail" src="{{ asset('images/' . $product->image) }}" alt="Card image cap">
			</div>
			<div class="col-md-6">
				<p class="card-text">{{ $product->description }}</p>
				<p class="card-text">
					<h3>Precio:
						<span class="badge badge-warning">
							${{ number_format($product->price, 2) }}
						</span>
					</h3>
				</p>
			</div>
		</div>
	</div>
	<div class="card-footer text-center">
		<a href="{{ route('home') }}" class="btn btn-primary">
			<i class="fas fa-arrow-circle-left"></i> Regresar
		</a>
	</div>
</div>