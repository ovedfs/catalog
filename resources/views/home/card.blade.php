<div class="card">
	<a href="{{ route('detail', $product) }}" title="Ver detalle">
		<img class="card-img-top" src="{{ asset('images/' . $product->image) }}" alt="Card image cap">
	</a>
	
	<div class="card-body">
		<h4 class="card-title text-center">
			{{ $product->name }}
			<a href="{{ route('category', $product->category) }}" class="btn btn-secondary btn-sm">
				{{ $product->category->name }}
			</a>
		</h4><hr>
		<p class="card-text">{{ $product->description }}</p>
		<p class="card-text">
			<h5>Precio:
				<span class="badge badge-warning">
					${{ number_format($product->price, 2) }}
				</span>
			</h5>
		</p>
	</div>
	<div class="card-footer text-center">
		<a href="{{ route('detail', $product) }}" class="btn btn-primary">
			Ver detalle <i class="fas fa-arrow-circle-right"></i>
		</a>
	</div>
</div>