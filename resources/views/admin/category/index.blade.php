@extends('layouts.back')

@section('content')
<div class="container">
	<div class="card">
		<div class="card-header">
			<h1 class="text-center">
				Admin Categorías
				<a href="{{ route('categories.create') }}" class="btn btn-primary">
					Agregar <i class="fas fa-plus-circle"></i>
				</a>
			</h1>		
		</div>
		<div class="card-body">
			@include('admin.category.table')
		</div>
	</div>
</div>
@endsection
