<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>#</th>
			<th>Nombre</th>
			<th>Descripción</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody>
		@forelse($categories as $category)
			<tr>
				<td>{{ $loop->index + 1 }}</td>
				<td>{{ $category->name }}</td>
				<td>{{ $category->description }}</td>
				<td>
					<a href="{{ route('categories.edit', $category) }}" class="btn btn-warning">
						<i class="fas fa-edit"></i>
					</a>

					{!! Form::open(['route' => ['categories.destroy', $category], 'style' => 'display:inline-block']) !!}
						<input type="hidden" name="_method" value="DELETE">
						<button type="submit" class="btn btn-danger">
							<i class="fas fa-trash-alt"></i>
						</button>
					{!! Form::close() !!}
				</td>
			</tr>
		@empty

		@endforelse
	</tbody>
</table>