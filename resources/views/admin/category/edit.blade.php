@extends('layouts.back')

@section('content')
<div class="container">
	<div class="card">
		<div class="card-header">
			<h1 class="text-center">
				Admin Categorías
				<small>[Editar Categoría]</small>
			</h1>		
		</div>
		<div class="card-body">
			<div class="row justify-content-md-center">
				<div class="col-md-6">
					@include('admin.category.form-edit')
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
