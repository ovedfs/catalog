@include('partials.errors')

{!! Form::open(['route' => 'categories.store']) !!}

	<div class="form-group">
		<label for="name">Nombre:</label>
		{{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nombre de la categoría...']) }}
	</div>

	<div class="form-group">
		<label for="description">Descripción:</label>
		{{ Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Descripción de la categoría...', 'rows' => 3]) }}
	</div>

	<div class="form-group">
		<a href="{{ route('categories.index') }}" class="btn btn-secondary">
			<i class="fas fa-arrow-circle-left"></i> Regresar
		</a>

		<button type="submit" class="btn btn-primary">
			Guardar <i class="fas fa-save"></i>
		</button>
	</div>

{!! Form::close() !!}