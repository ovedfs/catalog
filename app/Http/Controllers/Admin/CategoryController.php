<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;

class CategoryController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$categories = Category::all();

		return view('admin.category.index', compact('categories'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('admin.category.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$request->validate([
			'name' => 'required|unique:categories',
			'description' => 'required',
		]);

		$category = Category::create(['slug' => str_slug($request->name)] + $request->all());

		if($category){
			return redirect()
				->route('categories.index')
				->with('msg', 'Categoría creada');
		}

		return redirect()
			->route('categories.create')
			->withInput()
			->with('msg', 'Revisa los datos');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	/*public function show($id)
	{
		//
	}*/

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Category $category)
	{
		return view('admin.category.edit', compact('category'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Category $category)
	{
		$request->validate([
			'name' => 'required',
			'description' => 'required',
		]);

		$category->name = $request->name;
		$category->slug = str_slug($request->name);
		$category->description = $request->description;

		if($category->save()){
			return redirect()
				->route('categories.index')
				->with('msg', 'Categoría editada');
		}

		return redirect()
			->route('categories.edit', $category)
			->withInput()
			->with('msg', 'Revisa los datos');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Category $category)
	{
		if($category->delete()){
			return redirect()
				->route('categories.index')
				->with('msg', 'Categoría eliminada');
		}

		return redirect()
			->route('categories.index', $category)
			->with('msg', 'La categoría no se pudo eliminar');
	}
}
