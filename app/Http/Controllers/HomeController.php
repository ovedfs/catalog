<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;

class HomeController extends Controller
{
	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$products = Product::inRandomOrder()->paginate(6);
		return view('home.index', compact('products'));
	}

	public function detail(Product $product)
	{
		return view('home.detail', compact('product'));
	}

	public function category(Category $category)
	{
		return view('home.category', compact('category'));
	}
}
